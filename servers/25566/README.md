> [!TIP|style:flat|labelVisibility:hidden|iconVisibility:hidden]
> 基岩版客户端可加入  
> 死亡掉落  
> 单人岛屿，有范围限制  
> 可传送(传送扣除经验)  
> 无经济系统  
> 粘液科技及拓展  

## 指令

#### 空岛指令
- /is help

#### 传送
- /tpa 玩家名

#### 获取粘液科技书
- /sf guide

## 粘液科技附属拓展
|拓展名|
|----|
|[下界乌托邦](https://docs.sefiraat.dev/netheopoiesis/purification)|
|粘液匠魂|
|异域花园|
|至尊研究院|
|网络|
|无尽科技|