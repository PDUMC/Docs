>[!NOTE] 以下简称`BE`为基岩版，`JE`为JAVA版  
所有服务器地址均为`play.pdumc.top`

#### [BE娱乐服](servers/19132/)
> [!TIP|style:flat|labelVisibility:hidden|iconVisibility:hidden] 菜单内含跨服传送，可跳转至其他子服务器，服内有一些娱乐小游戏

> [!TIP|style:flat||label:现有小游戏|iconVisibility:hidden] 
> 起床战争  
> 空岛战争  
> 烫手山芋  
> PVP乱斗  

#### [BE原版生存服(七周目)](servers/19133/)
> [!TIP|style:flat|labelVisibility:hidden|iconVisibility:hidden]
> 原版纯净生存  
> 死亡掉落  
> 无经济系统  
> 无传送  
> 无OP及任何特殊权限玩家

#### [BE插件生存服](servers/19134/)
> [!TIP|style:flat|labelVisibility:hidden|iconVisibility:hidden]
> 可传送  
> 有经济系统和商店  
> 商店内购买头颅可抵消死亡掉落物品  

#### [BE空岛服](servers/54056/)
> [!TIP|style:flat|labelVisibility:hidden|iconVisibility:hidden] 
> 死亡不掉落  
> 有经济系统和商店  
> 可传送  

#### [JE原版生存互通服(二周目)](servers/25565/)
> [!TIP|style:flat|labelVisibility:hidden|iconVisibility:hidden]
> 基岩版客户端可加入  
> 死亡掉落  
> 原版纯净生存  
> 服内无OP及任何特殊权限玩家

#### [JE空岛互通服](servers/25566/)
> [!TIP|style:flat|labelVisibility:hidden|iconVisibility:hidden]
> 基岩版客户端可加入  
> 死亡掉落  
> 单人岛屿，有范围限制  
> 可传送(传送扣除经验)  
> 无经济系统  
> 粘液科技及拓展  
